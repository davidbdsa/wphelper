<?php
/**
 * BDSA WP HELPER
 * @version 1.0.2
 */
namespace BDSA;

function debug($v, $typeInfo = false, $debugBackTrace = false)
{
    $wpHelper = WPHelper::getInstance();
    $wpHelper->debug($v, $typeInfo, $debugBackTrace);
}

function br($subject, $search, $echo = true)
{
    $wpHelper = WPHelper::getInstance();
    return $wpHelper->br($subject, $search, $echo);
}

function sanitizePhoneNumber($stringToClean)
{
    $wpHelper = WPHelper::getInstance();
    return $wpHelper->sanitizePhoneNumber($stringToClean);
}

function getBasicHeader()
{
    $wpHelper = WPHelper::getInstance();
    return $wpHelper->getBasicHeader();
}

function getFullHeader($dir = 'templates/header')
{
    $wpHelper = WPHelper::getInstance();
    return $wpHelper->getFullHeader($dir);
}

function getBasicFooter()
{
    $wpHelper = WPHelper::getInstance();
    return $wpHelper->getBasicFooter();
}

function getFullFooter($dir = 'templates/footer')
{
    $wpHelper = WPHelper::getInstance();
    return $wpHelper->getFullFooter($dir);
}


class WPHelper
{

    const DEV_ENV = 'development';
    const PROD_ENV = 'production';
    const RATIO_16_9 = 16/9;
    const RATIO_4_3 = 4/3;
    const IS_CROPPED = true;
    const GENERIC_EXCERPT_SIZE = 25; // en mots - 25 mots

    private static $devDomain = '';
    private static $_instance = null;

    /**
     * Singleton
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

       // add_action('admin_init', array( self::$_instance, 'br' ), 10, 2);
       // add_action('admin_init', array( self::$_instance, 'sanitizePhoneNumber' ), 10, 1);

        return self::$_instance;
    }

    public function catchRoute($route, $callback)
    {
        add_action('template_redirect', function () use ($route, $callback) {
            global $wp;
            if ($wp->request == $route) {
                $callback();
            }
        });
    }
    
    /**
     * Permet de chercher dans les taxonomies (aussi custom)
     */
    public function extendSearchInTaxonomies()
    {
        add_filter('posts_where', function ($where) {
            global $wpdb;
            if (is_search()) {
                $where .= "OR (t.name LIKE '%".get_search_query()."%' AND {$wpdb->posts}.post_status = 'publish')";
            }
            return $where;
        });

        add_filter('posts_join', function ($join) {
            global $wpdb;
            if (is_search()) {
                $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
            }
            return $join;
        });

        add_filter('posts_groupby', function ($groupby) {
            global $wpdb;

            // we need to group on post ID
            $groupby_id = "{$wpdb->posts}.ID";
            if (!is_search() || strpos($groupby, $groupby_id) !== false) {
                return $groupby;
            }

            // groupby was empty, use ours
            if (!strlen(trim($groupby))) {
                return $groupby_id;
            }

          // wasn't empty, append ours
            return $groupby.", ".$groupby_id;
        });
    }
    
    /*
    * Ajoute des options d'amelioration de l'affichage du plugin ACF
    */
    public function acfOptimization($fieldTextName = 'my_flex_field', $fieldImageName = 'my_image_field')
    {

        //ajout de la valeur d'un txt field dans le nom d'un flexible
        add_filter('acf/fields/flexible_content/layout_title', function ($title, $field, $layout, $i) use ($fieldTextName, $fieldImageName) {
            //$title = '';
            // load sub field image
            // note you may need to add extra CSS to the page t style these elements

            if ($image = get_sub_field($fieldImageName)) {
                $title .= '<div class="thumbnail">';
                $title .= '<img src="' . $image['sizes']['thumbnail'] . '" height="36px" />';
                $title .= '</div>';
            }

            if ($text = get_sub_field($fieldTextName)) {
                $title .=  sprintf(' - <span class="flexible_title">%s</span>', $text)  ;
            }

            return $title;
        }, 10, 4);

        $styleCss ='
<style>
.acf-flexible-content .layout{
    border:5px solid #006799;
    border-radius: 10px;
}
.flexible_title{
    color: #006799;
}
</style>
        ';

        $this->addAdminInlineCss($styleCss);
    }

    /**
     * Ajout CSS inline dans l'adminn
     */
    public function addAdminInlineCss($css)
    {
        add_action('admin_head', function () use ($css) {
            echo $css;
        });
    }
    
    /**
     * Retourne ts les formats d'images disponibles
     */
    public function showAllImageFormats()
    {
        global $_wp_additional_image_sizes;
        self::getInstance()->debug($_wp_additional_image_sizes);
    }

    public function sanitizePhoneNumber($stringToClean, $echo = true)
    {
        $forbidden    = array(" ", "(0)");
        $clean_string = str_replace($forbidden, "", $stringToClean);
        if ($echo) {
            echo $clean_string;
        } else {
            return $clean_string;
        }
    }

    public function br($subject, $search, $echo = true)
    {
        $replace = '<br  />'.$search;
        $result = str_replace($search, $replace, $subject);
        if ($echo) {
            echo $result;
        } else {
            return $result;
        }
    }

    public function helloWorld()
    {
        var_dump('hello world');
    }

    /**
     * Inclu les CPT
     */
    public function loadCPTs($CPTCollection)
    {
        foreach ($CPTCollection as $cptFile) {
            if (file_exists($cptFile)) {
                require($cptFile);
            }
        }

        $this->addAllTaxosFilters();
    }

    public function filterPageTitle($callback)
    {
        add_filter('wp_title', $callback);
    }

    /**
     * Ajout un fichier CSS dans l'editeur admin
     * @param string $url l'adresse du fichier CSS
     */
    public function addAdminEditorCss($url)
    {
        add_action('admin_init', function () use ($url) {
             add_editor_style($url);
        });
    }

    /**
     * Ajout des styles dans le tiny MCE
     * @param [type] $styleFormats Collection de style
     */
    public function addAdminEditorFormats($styleFormats)
    {
        add_filter('tiny_mce_before_init', function ($initArray) use ($styleFormats) {
             $initArray['style_formats'] = json_encode($styleFormats);
            return $initArray;
        });
    }

    /**
     * Ajout de bouton dans tiny MCE
     * @param [type] $buttons [description]
     */
    public function addTinyMceButtons($_buttons = array())
    {
        add_filter('mce_buttons_2', function ($buttons) use ($_buttons) {
            array_unshift($buttons, 'styleselect');
            return $buttons;
        });
    }

    public function __construct($devDomain = 'macareux.io')
    {
        self::$devDomain = $devDomain;

        //ajout du support des thumbs
        add_theme_support('post-thumbnails');

        //ajout d'une page option
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page();
        }

        //add_action('wp_enqueue_scripts', [$this, 'hideSiteInfoCss']);

        //suppression du generator wordpress
        remove_action('wp_head', 'wp_generator');
    }


    public function hideSiteInfoCss()
    {
        $custom_css = "
            .site-info{
                    display: none;
            }";
        wp_add_inline_style('hide-site-info-style', $custom_css);
    }

    public function getAssetsUrl($directory = 'assets')
    {
        return get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . $directory;
    }

    public function getPublicUrl($value = '')
    {
        if (substr($value, 0, 1) !== '/') {
            throw new \Exception('Le chemin doit commencer par un slash.');
        }
        return $this->getThemeUrl() . $value;
    }

    public function getThemeUrl()
    {
        return get_stylesheet_directory_uri();
    }

    /**
     * retourne l'environnement
     * @return [type] [description]
     */
    public static function getEnv()
    {
        return strpos($_SERVER['HTTP_HOST'], self::$devDomain) !== false ? self::DEV_ENV : self::PROD_ENV;
    }

    public function isDevEnv()
    {
        return $this->getEnv() == self::DEV_ENV;
    }

    public static function enablePageExcerptSupport()
    {
        self::addPostTypeSupport('page', 'excerpt');
    }

    public function enableACFPageOption()
    {
        //ajout d'une page option
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page();
        }
    }

    // Filter except length to 35 words.
    public function setCustomExcerptLength($length)
    {
        add_filter('excerpt_length', function () use ($length) {
            return $length;
        }, 999);

        return $this;
    }

    public function registerMenus($menus)
    {
        /**
         * REGISTER MENU
         */
        add_action('init', function () use ($menus) {
            register_nav_menus($menus);
        });

        return $this;
    }


    /**
     * Ajout des formats d'images selon un format $format
     */
    public function addImageSizes($prefix, $format, $cropped = false)
    {
        //Formats d'image
        //$ratio16_9 = 16/9;
        //$ratio4_3 = 4/3;
        //j'ai préféré le format 16/9
        //ration de division de width/1.78
        //pour gestion des formats 16/9

        $formatWidths = [
            'large_size' => 1920,
            'big_size' => 1000,
            'med_size' => 600,
            'small_size' => 310,
            'tiny_size' => 140,
        ];

        foreach ($formatWidths as $k => $w) {
            $name = $prefix.'_'.$k;
            add_image_size($name, $w, $w/$format, $cropped);
        }
    }

    public function getFlattenedSubPageCollection($postId)
    {

        $my_wp_query  = new \WP_Query();
        $all_wp_pages = $my_wp_query->query(array(
                'post_type' => 'page',
                'posts_per_page' => '-1',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'visibilite_de_la_page_dans_le_sous-menu',
                        'value' => true,
                        'compare' => '!=',
                        'type' => 'BOOLEAN'
                    )
                ),
                'order_by' => 'weight',
                'order' => 'ASC'

            ));
        $children     = \get_page_children($postId, $all_wp_pages);

        //$this->debug($all_wp_pages);
        //$this->debug($children);

        return $children;
    }


    /**
     * A utiliser pour modifier la query
     */
    public function alterQuery($callback)
    {
        add_action('pre_get_posts', $callback);
    }


    public function overridePageTitle($callback)
    {
        add_filter('wp_title', $callback, 10, 2);
    }

    /**
     * Retourne mes items d'un menu en particulier
     */
    public function getMenuItems($taxo, $options = array())
    {
        if (empty($options)) {
            $options = array(

            );
        }
        if (!empty($taxo)) {
            $locations = \get_nav_menu_locations() ;
            $menu = \wp_get_nav_menu_object($locations[ $taxo ]);
            if ($menu instanceof \WP_Term) {
                $menuItems = \wp_get_nav_menu_items($menu->term_id, $options);
                if (is_array($menuItems)) {
                    $firstLevelItems = array();
                    foreach ($menuItems as $menuItem) {
                        if ($menuItem->post_parent == 0 && $menuItem->post_status == 'publish') {
                            $menuItem->subPages = $this->getFlattenedSubPageCollection($menuItem->object_id);
                            $firstLevelItems[]  = $menuItem;
                        }
                    }
                    return $firstLevelItems;
                } else {
                    return array();
                }
            }
        }
    }

    public function addPostTypesSupports(array $postTypesData):void
    {
        if (!empty($postTypesData)) {
            foreach ($postTypesData as $postTypeName => $featureName) {
                self::addPostTypeSupport($postTypeName, $featureName);
            }
        }
    }

    public static function addPostTypeSupport($postTypeName, $featureName)
    {
        add_post_type_support($postTypeName, $featureName);
    }

    public function flushCache()
    {
        if (self::getEnv()==self::DEV_ENV) {
            wp_cache_flush();
        }
    }

    public function phpVersion()
    {
        echo phpversion();
    }

    public function phpInfo()
    {
        phpInfo();
    }

    //ajout css sur le front
    public function addCssFiles($scripts, $ver = 1)
    {
        add_action('wp_enqueue_scripts', function () use ($scripts, $ver) {
            if ($this->isDevEnv()) {
                $ver = time();
            }
            foreach ($scripts as $key => $data) {
                wp_register_style($key, $data, false, $ver);
                wp_enqueue_style($key);
            }
        });
    }

    //ajout css sur le back
    public function addAdminCssFiles($scripts, $ver = 1)
    {
        add_action('admin_print_styles', function () use ($scripts, $ver) {
            if ($this->isDevEnv()) {
                $ver = time();
            }
            foreach ($scripts as $key => $data) {
                wp_register_style($key, $data, false, $ver);
                wp_enqueue_style($key);
            }
        });
    }

    public function addAdminJsScripts($scripts, $_hook)
    {
        add_action('admin_enqueue_scripts', function ($wp_hook) use ($scripts, $_hook) {
            if ($_hook !== $wp_hook) {
                return;
            }
            foreach ($scripts as $key => $uri) {
                //wp_register_style($key, $data, false, $ver);
                //wp_enqueue_style($key);
                wp_enqueue_script($key, $uri);
            }
        });
    }

    public function addJsScripts($scripts, $ver = 1)
    {
        add_action('wp_enqueue_scripts', function () use ($scripts, $ver) {
            $this->addJsFiles($scripts, $ver);
        });
    }

    public function addJsFilesToFrontpage($scripts, $ver = 1)
    {
        if (is_front_page()) {
            $this->addJsScripts($scripts, $ver);
        }
    }

    private function addJsFiles($scripts, $_ver = 1)
    {
        //add_action( 'wp_enqueue_scripts', function () use ($scripts, $ver){
        //$this->debug(is_front_page(), true);
        //$this->debug(get_queried_object());

        if ($this->isDevEnv()) {
            $ver = time();
        }

        foreach ($scripts as $key => $data) {
            if (is_array($data)) {
                //extract($data);

                $deps = $data['deps'] ?? null;
                $in_footer = $data['in_footer'] ?? false;
                $src = $data['src'] ?? null;
                $ver = $data['ver'] ?? null;

                if (is_null($ver)) {
                    if (self::isDevEnv()) {
                        $ver = time();
                    } else {
                        $ver = $_ver;
                    }
                }

                if (is_null($src)) {
                    return false;
                }
                if (is_null($in_footer)) {
                    $in_footer = false;
                }

                wp_register_script($key, $src, $deps, $ver, $in_footer);
            } else {
                wp_register_script($key, $data, null, $ver);
            }

            wp_enqueue_script($key);
        }
        //} );
    }

    public function addAttributesToPaginationLinks($attributesPrev, $attributesNext)
    {
        add_filter('next_posts_link_attributes', function () use ($attributesPrev) {
            return $this->getAttrArrayToString($attributesPrev);
        });
        add_filter('previous_posts_link_attributes', function () use ($attributesNext) {
            return $this->getAttrArrayToString($attributesNext);
        });
        /*
        function previous_posts_link_attributes()
        {
            return 'class="pagination-previous"';
        }

        function next_posts_link_attributes()
        {
            return 'class="pagination-next"';
        }
        */
    }

    private function getAttrArrayToString($attributes)
    {
        $attr = '';
        foreach ($attributes as $k => $v) {
            $attr .= $k.'="'.$v.'" ';
        }
        return $attr;
    }

    public function renderBasicHeaderFooter()
    {
        add_action('get_header', array($this, 'getBasicHeader'));
        add_action('get_footer', array($this, 'getBasicFooter'));
    }

    public function debug($v, $typeInfo = false, $debugBackTrace = false)
    {
        add_action('wp_footer', function () use ($v, $typeInfo, $debugBackTrace) {
            echo '<pre style="padding:50px 200px;background:#2b2b2b;color:darkseagreen;font-size:16px;">';
            //echo '# Toc toc<br />';
            echo '# Debug de ' . $_SERVER['HTTP_HOST'] . '...<br />';

            if (!empty($v)) {
                if ($typeInfo) {
                    var_dump($v);
                } else {
                    print_r($v);
                }
            } else {
                echo '<span style="color:white;">--- Valeur vide ---</span><br />';
            }

            if ($debugBackTrace) {
                echo '<br /># Résultats de la fonction debug_backtrace()<br />';
                print_r(debug_backtrace());
            }

            echo '</pre>';
        });
    }

    public function debugWithTypes($v)
    {
        $this->debug($v, true);
    }

    /**
     * A appeler au sein d'un template
     */
    public function getFileNameOnly($file, $withHtmlComment)
    {
        $fileName = strrchr($file, '/');
        if ($withHtmlComment) {
            return '<!--' .$fileName. '-->';
        }
        return $fileName;
    }

    /**
     * Retourne un header simple
     */
    public function getBasicHeader()
    {
        ob_start();
        ?>
<!DOCTYPE html>
<html lang="<?php bloginfo('language') ?>">
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
        <?php wp_head() ?>
    </head>
    <body <?php body_class() ?>>

        <?php

        $header = ob_get_contents();
        ob_end_clean();

        echo $header;
    }

    /**
     * Retourne un footer basic
     */
    public function getBasicFooter()
    {
         ob_start(); ?>
<?php wp_footer() ?>
</body>
</html><?php

        $footer = ob_get_contents();
        ob_end_clean();

        echo $footer;
    }
    
    public function getFullHeader($dir = 'templates/header')
    {
        $this->getBasicHeader();
        get_template_part($dir);
    }
    
    
    public function getFullFooter($dir = 'templates/footer')
    {
        get_template_part($dir);
        $this->getBasicFooter();
    }
    
    public function renderMenu($menu, $container = 'defaultContainer')
    {
        wp_nav_menu([
            'theme_location' => $menu,
            'container' => $container
        ]);
    }

    //Ajout des filtres par taxonomies pour chaque Custom Post Type
    private function addAllTaxosFilters() {
        add_action( 'init', function () {
            $args = array(
                'public'   => true,
                '_builtin' => false
            );

            $output   = 'names'; // names or objects, note names is the default
            $operator = 'and'; // 'and' or 'or'

            $post_types = get_post_types( $args, $output, $operator );

            $forbidden = array(
                'language',
                'post_translations'
            );

            foreach ( $post_types as $post_type ) {
                $taxonomy_objects = get_object_taxonomies( $post_type );

                foreach ( $taxonomy_objects as $taxo ) {
                    if ( ! in_array( $taxo, $forbidden ) ) {
                        $this->addFilterTaxoAdmin( $post_type, $taxo );
                    }
                }
            }
        } );
    }

    //Fonction ajoutant des filtres en fonction des Custom Post Type et des Custom Taxonomies
    private function addFilterTaxoAdmin( $cpt, $taxo ) {
        add_action( 'restrict_manage_posts', function () use ( $cpt, $taxo ) {
            global $typenow;
            $post_type = $cpt;
            $taxonomy  = $taxo;
            if ( $typenow == $post_type ) {
                $selected      = isset( $_GET[ $taxonomy ] ) ? $_GET[ $taxonomy ] : '';
                $info_taxonomy = get_taxonomy( $taxonomy );
                wp_dropdown_categories( array(
                    'show_option_all' => __( "{$info_taxonomy->label}" ),
                    'taxonomy'        => $taxonomy,
                    'name'            => $taxonomy,
                    'orderby'         => 'name',
                    'selected'        => $selected,
                    'show_count'      => true,
                    'hide_empty'      => true,
                ) );
            };
        } );

        add_filter( 'parse_query', function ( $query ) use ( $cpt, $taxo ) {
            global $pagenow;
            $post_type = $cpt;
            $taxonomy  = $taxo;
            $q_vars    = &$query->query_vars;
            if ( $pagenow == 'edit.php' && isset( $q_vars['post_type'] ) && $q_vars['post_type'] == $post_type && isset( $q_vars[ $taxonomy ] ) && is_numeric( $q_vars[ $taxonomy ] ) && $q_vars[ $taxonomy ] != 0 ) {
                $term                = get_term_by( 'id', $q_vars[ $taxonomy ], $taxonomy );
                $q_vars[ $taxonomy ] = $term->slug;
            }
        } );
    }
}



